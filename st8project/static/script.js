$(document).ready(()=>{
    $('#button').click(function(){
        var key= $('#search').val();

        $.ajax({
            method:'GET',
            url :'http://localhost:8000/getData?key=' + key,
            success:function(response){

                let bookS = response.items


                for(let i=0;i<response.items.length;i++) {
                    let book = bookS[i].volumeInfo
                    var name = $('<td>').text(book.title);

                    if ('publisher' in book == false) var publisher = $('<td>').text('-');
                    else var publisher = $('<td>').text(book.publisher);

                    if ('pageCount' in book == false) var pageCount =$('<td>').text("-");
                    else var pageCount = $('<td>').text(book.pageCount);

                    if ('authors' in book == false) var authors =$('<td>').text("-");
                    else var authors = $('<td>').text(book.authors);

                    if ('categories' in book == false) var categories= $('<td>').text("-");
                    else var categories = $('<td>').text(book.categories);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append(name, authors,
                        pageCount, publisher, categories, img);

                    $('tbody').append(tr);

                }


            }

        })
    })
})