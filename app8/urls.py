from django.contrib import admin 
from django.urls import path, include 
from . views import index, get_data

urlpatterns =[
    path('', index),
    path('getData/', get_data)
]