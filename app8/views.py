from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests 


def index(req):
    return render(req, 'index.html')
def get_data(req):
    key = req.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=<keyword>'+ key
    response = requests.get(url)
    response_json= response.json()
    return JsonResponse(response_json)